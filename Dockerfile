## Build for code-inspector

## specify your base image pulled from docker hub
FROM scardon/ruby-node-alpine:2.5.1

## specify the group or individual maintaining image
LABEL maintainer="Mike Atkinson"

RUN apk update && apk upgrade && apk add --no-cache --update \
  bash \
  file \
  git \
  jq \
  python3 \
  python3-dev \
&& rm -rf /var/lib/apt/lists/*

# pip3 needs to be run initialy to upgrade pip
RUN pip3 install --upgrade pip
COPY requirements.txt .
RUN pip install -r requirements.txt
